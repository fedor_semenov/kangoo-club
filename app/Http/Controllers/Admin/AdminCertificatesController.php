<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Certificate;
use App\Models\CertificateLocalization;

class AdminCertificatesController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Лицензии и сертфикаты';
        
        $certificates = Certificate::orderBy('position')->get();
        
        return view('admin.Certificates.showCertificates', compact(['title', 'certificates']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( $errors = Certificate::upload_images($request->file('images')) )
            return redirect()->back()->with('errors', $errors);
        else
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Редактирование информации о сертификате ID - ' . $id;

        $post = Certificate::whereId($id)->with('localizations')->first();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        return view('admin.Certificates.editCertificate', compact(['title', 'post', 'data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $certificate = Certificate::findOrFail($id);
        $certificate->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $certificate->localizations()
                            ->whereLang($language->code)
                            ->updateOrCreate(['lang' => $language->code, 'certificate_id' => $certificate->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Информация обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $certificates_ids = $request->get('check');

        $certificates = Certificate::whereIn('id', $certificates_ids)->get();
        
        if( count($certificates) )
        {
            foreach( $certificates as $certificate )
            {
                \File::delete( public_path('uploads/certificates/' . $certificate->image) );
                \File::delete( public_path('uploads/certificates/middle/' . $certificate->image) );
            }
        }

        Certificate::destroy( $certificates_ids );

        return redirect()->back();
    }

    /**
     * Visible certificate
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $certificate = Certificate::find( $id );
            
            $visible = (empty($certificate->visible) ? 1 : 0);
            
            $certificate->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Sortable certificates
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                Certificate::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
