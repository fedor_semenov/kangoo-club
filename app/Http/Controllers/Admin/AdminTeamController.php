<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\TeamRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Team;
use App\Models\TeamLocalization;

class AdminTeamController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Наша команда';
        
        $posts = Team::select('as_team.*')
                     ->localName(app()->getLocale())
                     ->orderBy('as_team.position')
                     ->get();
        
        return view('admin.Team.showTeam', compact(['title', 'posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление сотрудника';

        $post = new Team;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new TeamLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/team');

        return view('admin.Team.editTeam', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));

        $post = Team::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $post->localizations()->create( $info );
        }

        // If upload image
        if( $image = $request->file('image') )
            Team::imageUpload( $image, $post->id );
        
        return redirect( asset('master/team/' . $post->id) )->with('success', 'Сотрудник добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование данных сотрудника";

        $post = Team::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/team/' . $id);

        return view('admin.Team.editTeam', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $post = Team::findOrFail($id);
        $post->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $post->localizations()
                     ->whereLang($language->code)
                     ->updateOrCreate(['lang' => $language->code, 'post_id' => $post->id], $request->{$language->code});
            }
        }

        // If upload image
        if( $image = $request->file('image') )
            Team::imageUpload( $image, $id );

        return redirect()->back()->with('success', 'Данные сотрудника обновлены');
    }

    /**
     * Destroy team items
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');

        if( $posts = Team::whereIn('id', $ids)->get() )
        {
            foreach( $posts as $post )
            {
                \File::delete( public_path('uploads/team/' . $post->image) );
            }
        }

        Team::destroy( $ids );
        
        return redirect()->back();
    }

    /**
     * Visible team item
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $post = Team::find( $id );
            
            $visible = (empty($post->visible) ? 1 : 0);
            
            $post->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Team sortable
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                Team::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
