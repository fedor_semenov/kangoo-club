<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProductFeatureRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\ProductFeature;
use App\Models\ProductFeatureLocalization;

class AdminProductsFeaturesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Характеристики';
        
        $features = ProductFeature::select('as_products_features.*')
                                  ->localName(app()->getLocale())
                                  ->orderBy('as_products_features.position')
                                  ->get();
        
        return view('admin.Products.showProductsFeatures', compact(['title', 'features']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление характеристики';

        $post = new ProductFeature;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new ProductFeatureLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/products-features');

        return view('admin.Products.editProductsFeature', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFeatureRequest $request)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));

        $feature = ProductFeature::create( $input );
        
        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $feature->localizations()->create( $info );
        }

        return redirect( asset('master/products-features/' . $feature->id) )->with('success', 'Характеристика добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Редактирование характеристики';
        
        $post = ProductFeature::whereId($id)->with('localizations')->first();

        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }
        
        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/products-features/' . $id);

        return view('admin.Products.editProductsFeature', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductFeatureRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $feature = ProductFeature::find( $id );
        $feature->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $feature->localizations()
                        ->whereLang($language->code)
                        ->updateOrCreate(['lang' => $language->code, 'feature_id' => $feature->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Информация успешно обновлена');
    }

    /**
     * Remove fetures
     *
     * @return redirect
     */
    public function destroy(Request $request)
    {
        ProductFeature::destroy( $request->get('check') );

        return redirect()->back();
    }

    /**
     * Visible feature
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $feature = ProductFeature::find( $id );
            
            $visible = (empty($feature->visible) ? 1 : 0);
            
            $feature->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Sortable features
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                ProductFeature::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
