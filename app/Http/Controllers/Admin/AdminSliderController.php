<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\MainSlider;

class AdminSliderController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Слайдер';
        
        $images = MainSlider::orderBy('position')->get();
        
        return view('admin.showSlider', compact(['title', 'images']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( $errors = MainSlider::upload_images($request->file('images')) )
            return redirect()->back()->with('errors', $errors);
        else
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $images_ids = $request->get('check');

        $images = MainSlider::whereIn('id', $images_ids)->get();
        
        if( count($images) )
        {
            foreach( $images as $item )
            {
                \File::delete( public_path() . '/uploads/main_slider/' . $item->image );
            }
        }

        MainSlider::destroy( $images_ids );

        return redirect()->back();
    }

    /**
     * Visible image
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $slider = MainSlider::find( $id );
            
            $visible = (empty($slider->visible) ? 1 : 0);
            
            $slider->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Sortable images
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                MainSlider::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
