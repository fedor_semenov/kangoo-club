<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProductOptionRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\ProductOption;
use App\Models\ProductOptionLocalization;
use App\Models\ProductFeature;

class AdminProductsOptionsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex( $feature_id )
    {
        $title = 'Опции';
        
        $options = ProductOption::select('as_products_options.*')
                                ->localName(app()->getLocale())
                                ->where('as_products_options.feature_id', $feature_id)
                                ->paginate(25);

        $feature_name = ProductFeature::select('as_products_features.*')
                                      ->localName(app()->getLocale())
                                      ->where('as_products_features.id', $feature_id)
                                      ->first()
                                      ->name;
        
        return view('admin.Products.showProductsOptions', compact(['title', 'options', 'feature_id', 'feature_name']));
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function postIndex( Request $request )
    {
        ProductOption::destroy( $request->get('check') );

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdd( $feature_id )
    {
        $title = 'Добавление опции';

        $post = new ProductOption;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new ProductOptionLocalization;
        }

        $feature_name = ProductFeature::select('as_products_features.*')
                                      ->localName(app()->getLocale())
                                      ->where('as_products_features.id', $feature_id)
                                      ->first()
                                      ->name;

        return view('admin.Products.editProductsOption', compact(['title', 'post', 'data', 'feature_id', 'feature_name']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postAdd( ProductOptionRequest $request, $feature_id )
    {
        $option = ProductOption::create(['feature_id' => $feature_id]);
        
        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $option->localizations()->create( $info );
        }
        
        return redirect( asset('master/products-options/' . $feature_id . '/edit/' . $option->id) )->with('success', 'Опция добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit( $feature_id, $option_id )
    {
        $title = 'Редактирование опции';
        
        $post = ProductOption::whereId($option_id)->with('localizations')->first();

        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        $feature_name = ProductFeature::select('as_products_features.*')
                                      ->localName(app()->getLocale())
                                      ->where('as_products_features.id', $feature_id)
                                      ->first()
                                      ->name;

        return view('admin.Products.editProductsOption', compact(['title', 'post', 'data', 'feature_id', 'feature_name']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postEdit( ProductOptionRequest $request, $feature_id, $option_id )
    {
        $option = ProductOption::find( $option_id );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $option->localizations()
                       ->whereLang($language->code)
                       ->updateOrCreate(['lang' => $language->code, 'option_id' => $option->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Информация успешно обновлена');
    }
}
