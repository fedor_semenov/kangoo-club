<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Feedback;

class AdminFeedbacksController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Обратная связь';

        $posts = Feedback::orderBy('created_at', 'DESC')->paginate(25);

        return view('admin.showFeedbacks', compact(['title', 'posts']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ids = $request->get('check');
        
        if( $request->get('action') == 'delete' )
            Feedback::destroy( $ids );

        return redirect()->back();
    }

    /**
     * Visible message
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $post = Feedback::find( $id );
            
            $visible = (empty($post->visible) ? 1 : 0);
            
            $post->update(['visible' => $visible]);

            $count_messages = Feedback::whereVisible(0)->count();
            
            return \Response::json(['count_messages' => intval($count_messages)]);
        }
    }
}
