<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\VideoGalleryAlbumRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\VideoGalleryAlbum;
use App\Models\VideoGalleryAlbumLocalization;
use App\Models\VideoGalleryItem;

class AdminVideoGalleryAlbumsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Альбомы';
        
        $albums = VideoGalleryAlbum::select('as_video_gallery_albums.*')
                                   ->localName(app()->getLocale())
                                   ->orderBy('as_video_gallery_albums.position')
                                   ->get();
        
        return view('admin.VideoGallery.showAlbums', compact(['title', 'albums']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление альбома';

        $post = new VideoGalleryAlbum;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new VideoGalleryAlbumLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/video-gallery-albums');
        
        return view('admin.VideoGallery.editAlbum', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoGalleryAlbumRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));

        $album = VideoGalleryAlbum::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $album->localizations()->create( $info );
        }

        // If upload image
        if( $image = $request->file('image') )
            VideoGalleryAlbum::imageUpload( $image, $album->id );

        return redirect( asset('master/video-gallery-albums/' . $album->id) )->with('success', 'Альбом добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование альбома";

        $post = VideoGalleryAlbum::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/video-gallery-albums/' . $id);

        return view('admin.VideoGallery.editAlbum', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VideoGalleryAlbumRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $album = VideoGalleryAlbum::findOrFail($id);
        $album->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $album->localizations()
                      ->whereLang($language->code)
                      ->updateOrCreate(['lang' => $language->code, 'album_id' => $album->id], $request->{$language->code});
            }
        }

        // If upload image
        if( $image = $request->file('image') )
            VideoGalleryAlbum::imageUpload( $image, $id );

        return redirect()->back()->with('success', 'Альбом обновлён');
    }

    /**
     * Destroy albums items
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');

        if( $albums = VideoGalleryAlbum::whereIn('id', $ids)->get() )
        {
            foreach( $albums as $album )
            {
                \File::delete( public_path('uploads/video-gallery/albums/' . $album->image) );
            }

            if( $items = VideoGalleryItem::whereIn('album_id', $ids)->get() )
            {
                foreach( $items as $item )
                {
                    \File::delete( public_path('uploads/video-gallery/images/' . $item->image) );
                }
            }
        }

        VideoGalleryAlbum::destroy( $ids );
        
        return redirect()->back();
    }

    /**
     * Visible album item
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $post = VideoGalleryAlbum::find( $id );
            
            $visible = (empty($post->visible) ? 1 : 0);
            
            $post->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Albums sortable
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                VideoGalleryAlbum::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
