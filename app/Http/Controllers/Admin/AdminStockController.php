<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\StockRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Stock;
use App\Models\StockLocalization;

use Carbon\Carbon;

class AdminStockController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Акции';
        
        $posts = Stock::select('as_stock.*')
                      ->localName(app()->getLocale())
                      ->orderBy('as_stock.date', 'DESC')
                      ->paginate(25);
        
        return view('admin.Stock.showStock', compact(['title', 'posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление акции';

        $post = new Stock;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new StockLocalization;
        }

        $date = Stock::getDate( Carbon::now()->toDateTimeString() );

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/stock');
        
        return view('admin.Stock.editStock', compact(['title', 'post', 'data', 'date', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StockRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));
        
        if( isset($input['date']) )
            $input['date'] = Carbon::createFromFormat('d.m.y', $input['date'])->toDateTimeString();

        $post = Stock::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $post->localizations()->create( $info );
        }

        // If upload image
        if( $request->file('image') )
            Stock::imageUpload( $request->file('image'), $post->id );

        return redirect( asset('master/stock/' . $post->id) )->with('success', 'Акция добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование акции";

        $post = Stock::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        $date = Stock::getDate( $post->date );

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/stock/' . $id);

        return view('admin.Stock.editStock', compact(['title', 'post', 'data', 'date', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StockRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));
        
        if( isset($input['date']) )
            $input['date'] = Carbon::createFromFormat('d.m.y', $input['date'])->toDateTimeString();

        $post = Stock::findOrFail($id);
        $post->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $post->localizations()
                     ->whereLang($language->code)
                     ->updateOrCreate(['lang' => $language->code, 'post_id' => $post->id], $request->{$language->code});
            }
        }

        // If upload image
        if( $request->file('image') )
            Stock::imageUpload( $request->file('image'), $id );

        return redirect()->back()->with('success', 'Акция обновлена');
    }

    /**
     * Destroy posts
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');

        if( $posts = Stock::whereIn('id', $ids)->get() )
        {
            foreach( $posts as $post )
            {
                \File::delete( public_path('uploads/stock/' . $post->image) );
            }
        }

        Stock::destroy( $ids );
        
        return redirect()->back();
    }

    /**
     * Visible post
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $post = Stock::find( $id );
            
            $visible = (empty($post->visible) ? 1 : 0);
            
            $post->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }
}
