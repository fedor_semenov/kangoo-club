<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Partner;
use App\Models\PartnerLocalization;

class AdminPartnersController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Партнёры';
        
        $partners = Partner::orderBy('position')->get();
        
        return view('admin.Partners.showPartners', compact(['title', 'partners']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( $errors = Partner::upload_images($request->file('images')) )
            return redirect()->back()->with('errors', $errors);
        else
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Редактирование данных о партнёре ID - ' . $id;

        $post = Partner::whereId($id)->with('localizations')->first();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        return view('admin.Partners.editPartner', compact(['title', 'post', 'data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $partner = Partner::findOrFail($id);
        $partner->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $partner->localizations()
                        ->whereLang($language->code)
                        ->updateOrCreate(['lang' => $language->code, 'partner_id' => $partner->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Информация обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $partners_ids = $request->get('check');

        $partners = Partner::whereIn('id', $partners_ids)->get();
        
        if( count($partners) )
        {
            foreach( $partners as $partner )
            {
                \File::delete( public_path('uploads/partners/' . $partner->image) );
            }
        }

        Partner::destroy( $partners_ids );

        return redirect()->back();
    }

    /**
     * Visible partner
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $partner = Partner::find( $id );
            
            $visible = (empty($partner->visible) ? 1 : 0);
            
            $partner->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Sortable partners
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                Partner::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
