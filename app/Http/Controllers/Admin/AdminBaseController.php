<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Language;
use App\Models\Product;
use App\Models\ProductOrder;
use App\Models\ProductFeature;
use App\Models\Review;
use App\Models\Feedback;

class AdminBaseController extends Controller
{
    protected $languages;

    public function __construct()
    {
		$this->languages = $languages = Language::get();

		$reviews_count = Review::count();

		$feedbacks_count = Feedback::whereVisible(0)->count();

		view()->share(compact(['languages', 'reviews_count', 'feedbacks_count']));
	}

	// Products menu
	public function getProductsMenu()
	{
		$title = 'Магазин';

		$products_count = Product::count();

		$products_orders_count = ProductOrder::count();

		$products_features_count = ProductFeature::count();

		return view('admin.Products.showProductsMenu', compact(['title', 'products_count', 'products_orders_count', 'products_features_count']));
	}
}
