<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Partner;

class PartnersController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'partners')
                    ->firstOrFail();

        $partners = Partner::select('as_partners.*')
                           ->localName(app()->getLocale())
                           ->where('as_partners.visible', 1)
                           ->orderBy('as_partners.position')
                           ->get();

        return view('frontend.showPartners', compact(['page', 'partners']));
    }
}
