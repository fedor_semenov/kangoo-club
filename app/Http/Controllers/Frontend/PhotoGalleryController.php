<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\PhotoGalleryAlbum;
use App\Models\PhotoGalleryImage;

class PhotoGalleryController extends BaseController
{
    private $photo_gallery;

    public function __construct()
    {
        parent::__construct();

        $this->photo_gallery = Page::select('as_pages.*')
                                   ->localAll(app()->getLocale())
                                   ->where('as_pages.slug', '=', 'photo-gallery')
                                   ->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $this->photo_gallery;

        $albums = PhotoGalleryAlbum::select('as_photo_gallery_albums.*');

        $albums_count = PhotoGalleryAlbum::whereVisible(1)->count();

        if( $request->ajax() )
        {
            $locale = $request->get('locale');

            \App::setlocale($locale);

            $offset = (int)$request->get('offset');

            $albums = $albums->localAll($locale)
                             ->where('as_photo_gallery_albums.visible', 1)
                             ->offset($offset)
                             ->limit(1)
                             ->orderBy('as_photo_gallery_albums.position')
                             ->get();

            return response()->json([
                                'albums' => view('frontend.components._gallery_albums', ['albums' => $albums, 'type' => 'photo'])->render(),
                                'offset' => ($offset + 1),
                                'albums_count' => $albums_count
                            ]);
        }
        else
        {
            $albums = $albums->localAll(app()->getLocale())
                             ->where('as_photo_gallery_albums.visible', 1)
                             ->orderBy('as_photo_gallery_albums.position')
                             ->take(8)
                             ->get();

            return view('frontend.showPhotoGalleryAlbums', compact(['page', 'albums', 'albums_count']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = PhotoGalleryAlbum::select('as_photo_gallery_albums.*')
                                 ->localAll(app()->getLocale())
                                 ->where('as_photo_gallery_albums.slug', '=', $slug)
                                 ->firstOrFail();

        $images = PhotoGalleryImage::select('as_photo_gallery_images.*')
                                   ->localName(app()->getLocale())
                                   ->where('as_photo_gallery_images.album_id', '=', $page->id)
                                   ->where('as_photo_gallery_images.visible', '=', 1)
                                   ->orderBy('as_photo_gallery_images.position')
                                   ->paginate(18);

        $breadcrumbs = [(object)['name' => $this->photo_gallery->name, 'url' => $this->photo_gallery->slug]];

        return view('frontend.showPhotoGalleryImages', compact(['page', 'images', 'breadcrumbs']));
    }
}
