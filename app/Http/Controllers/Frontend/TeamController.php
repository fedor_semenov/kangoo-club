<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Team;

class TeamController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'team')
                    ->firstOrFail();

        $team = Team::select('as_team.*')
                    ->localAll(app()->getLocale())
                    ->where('as_team.visible', 1)
                    ->orderBy('as_team.position')
                    ->get();

        return view('frontend.showTeam', compact(['page', 'team']));
    }
}
