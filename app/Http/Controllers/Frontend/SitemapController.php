<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Service;
use App\Models\News;
use App\Models\Article;
use App\Models\Stock;
use App\Models\PhotoGalleryAlbum;
use App\Models\VideoGalleryAlbum;

class SitemapController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'sitemap')
                    ->firstOrFail();

        $pages = Page::select('as_pages.*')
                     ->localName(app()->getLocale())
                     ->whereNotIn('slug', ['404', 'sitemap', 'search'])
                     ->get();

        $services = Service::select('as_services.*')
                           ->localName(app()->getLocale())
                           ->where('as_services.visible', 1)
                           ->orderBy('as_services.position')
                           ->get();

        $news = News::select('as_news.*')
                    ->localName(app()->getLocale())
                    ->where('as_news.visible', 1)
                    ->orderBy('as_news.date', 'DESC')
                    ->get();

        $articles = Article::select('as_articles.*')
                           ->localName(app()->getLocale())
                           ->where('as_articles.visible', 1)
                           ->orderBy('as_articles.id', 'DESC')
                           ->get();

        $stock = Stock::select('as_stock.*')
                      ->localName(app()->getLocale())
                      ->where('as_stock.visible', 1)
                      ->orderBy('as_stock.date', 'DESC')
                      ->get();

        $photo_gallery = PhotoGalleryAlbum::select('as_photo_gallery_albums.*')
                                          ->localName(app()->getLocale())
                                          ->where('as_photo_gallery_albums.visible', 1)
                                          ->orderBy('as_photo_gallery_albums.position')
                                          ->get();

        $video_gallery = VideoGalleryAlbum::select('as_video_gallery_albums.*')
                                          ->localName(app()->getLocale())
                                          ->where('as_video_gallery_albums.visible', 1)
                                          ->orderBy('as_video_gallery_albums.position')
                                          ->get();

        return view('frontend.showSitemap', compact([
                                                        'page',
                                                        'pages',
                                                        'services',
                                                        'news',
                                                        'articles',
                                                        'stock',
                                                        'photo_gallery',
                                                        'video_gallery'
                                                    ]));
    }
}
