<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Certificate;

class CertificatesController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'certificates')
                    ->firstOrFail();

        $certificates = Certificate::select('as_certificates.*')
                                   ->localName(app()->getLocale())
                                   ->where('as_certificates.visible', 1)
                                   ->orderBy('as_certificates.position')
                                   ->get();

        return view('frontend.showCertificates', compact(['page', 'certificates']));
    }
}
