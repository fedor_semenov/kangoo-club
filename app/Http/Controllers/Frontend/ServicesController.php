<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Service;

class ServicesController extends BaseController
{
    private $services;

    public function __construct()
    {
        parent::__construct();

        $this->services = Page::select('as_pages.*')
                              ->localAll(app()->getLocale())
                              ->where('as_pages.slug', '=', 'services')
                              ->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->services;

        $services = Service::select('as_services.*')
                           ->localAll(app()->getLocale())
                           ->where('as_services.visible', 1)
                           ->orderBy('as_services.position')
                           ->paginate(5);

        return view('frontend.showServices', compact(['page', 'services']));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = Service::select('as_services.*')
                       ->localAll(app()->getLocale())
                       ->where('as_services.slug', '=', $slug)
                       ->firstOrFail();

        $breadcrumbs = [(object)['name' => $this->services->name, 'url' => $this->services->slug]];

        return view('frontend.showPage', compact(['page', 'breadcrumbs']));
    }
}
