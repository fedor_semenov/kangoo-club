<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Stock;

class StockController extends BaseController
{
    private $stock;

    public function __construct()
    {
        parent::__construct();

        $this->stock = Page::select('as_pages.*')
                           ->localAll(app()->getLocale())
                           ->where('as_pages.slug', '=', 'stock')
                           ->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->stock;

        $posts = Stock::select('as_stock.*')
                      ->localAll(app()->getLocale())
                      ->where('as_stock.visible', 1)
                      ->orderBy('as_stock.date', 'DESC')
                      ->paginate(8);

        return view('frontend.showStock', compact(['page', 'posts']));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = Stock::select('as_stock.*')
                     ->localAll(app()->getLocale())
                     ->where('as_stock.slug', '=', $slug)
                     ->firstOrFail();

        $breadcrumbs = [(object)['name' => $this->stock->name, 'url' => $this->stock->slug]];

        return view('frontend.showPage', compact(['page', 'breadcrumbs']));
    }
}
