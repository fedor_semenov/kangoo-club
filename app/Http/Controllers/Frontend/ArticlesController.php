<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Article;

class ArticlesController extends BaseController
{
    private $articles;

    public function __construct()
    {
        parent::__construct();

        $this->articles = Page::select('as_pages.*')
                              ->localAll(app()->getLocale())
                              ->where('as_pages.slug', '=', 'articles')
                              ->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->articles;

        $posts = Article::select('as_articles.*')
                        ->localAll(app()->getLocale())
                        ->where('as_articles.visible', 1)
                        ->orderBy('as_articles.id', 'DESC')
                        ->paginate(24);

        $posts_type = 'articles';

        return view('frontend.showPosts', compact(['page', 'posts', 'posts_type']));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = Article::select('as_articles.*')
                       ->localAll(app()->getLocale())
                       ->where('as_articles.slug', '=', $slug)
                       ->firstOrFail();

        $breadcrumbs = [(object)['name' => $this->articles->name, 'url' => $this->articles->slug]];

        return view('frontend.showPage', compact(['page', 'breadcrumbs']));
    }
}
