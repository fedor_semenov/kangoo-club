<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\ProductOrderRequest;

use App\Http\Controllers\Frontend\BaseController;

use App\Mail\ProductOrderMail;
use Illuminate\Support\Facades\Mail;

use App\Models\Page;
use App\Models\Product;
use App\Models\ProductOrder;
use App\Models\Setting;

class ProductsController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'products')
                    ->firstOrFail();

        $products = Product::with(['product_images' => function($query){
                                $query->whereVisible(1)
                                      ->orderBy('position');
                             }])
                           ->with(['product_features' => function($query){
                                $query->leftJoin('as_products_features', 'as_products_features.id', '=', 'as_products_features_options.feature_id')
                                      ->leftJoin('as_products_features_localization', 'as_products_features_localization.feature_id', '=', 'as_products_features.id')
                                      ->leftJoin('as_products_options', 'as_products_options.id', '=', 'as_products_features_options.option_id')
                                      ->leftJoin('as_products_options_localization', 'as_products_options_localization.option_id', '=', 'as_products_options.id')
                                      ->select([
                                            'as_products_features_options.*',
                                            'as_products_features_localization.name AS feature_name',
                                            'as_products_options_localization.name AS option_name'
                                        ])
                                      ->where('as_products_features_localization.lang', app()->getLocale())
                                      ->where('as_products_options_localization.lang', app()->getLocale())
                                      ->where('as_products_features.visible', 1)
                                      ->orderBy('as_products_features.position');
                             }])
                           ->select('as_products.*')
                           ->localName(app()->getLocale())
                           ->where('as_products.visible', 1)
                           ->orderBy('as_products.position')
                           ->paginate(18);

        return view('frontend.showProducts', compact(['page', 'products']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductOrderRequest $request)
    {
        if( $request->ajax() )
        {
            $fields = $request->except(['_token', 'locale']);
            
            $order = ProductOrder::create($fields);

            $email = Setting::getSettings()->email;

            $host = $request->getHost();

            Mail::to($email)->send(new ProductOrderMail(array_merge($fields, ['order_id' => $order->id]), $host));

            return response()->json(['success' => __('design.message_order')], 200);
        }
    }
}
