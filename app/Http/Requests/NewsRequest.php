<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'    => (!empty(\Request::segment(3)) ? 'unique:as_news,slug,' . (int)\Request::segment(3) : 'unique:as_news'),
            'image'   => 'image',
            'ru.name' => 'required'
        ];
    }
}
