<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReviewMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Illuminate\Http\Request instance.
     *
     * @var Request
     */
    protected $fields = [];
    protected $host = '';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fields, $host)
    {
        $this->fields = $fields;
        $this->host = $host;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@' . $this->host, 'Kangoo Club ZP')
                    ->subject('Новый отзыв')
                    ->view('emails.review')
                    ->with($this->fields);
    }
}
