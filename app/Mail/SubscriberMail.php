<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriberMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Illuminate\Http\Request instance.
     *
     * @var Request
     */
    protected $email = '';
    protected $host = '';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $host)
    {
        $this->email = $email;
        $this->host = $host;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@' . $this->host, 'Kangoo Club ZP')
                    ->subject('Подписка')
                    ->view('emails.subscribe')
                    ->with(['email' => $this->email]);
    }
}
