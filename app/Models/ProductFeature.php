<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFeature extends Model
{
    protected $table = 'as_products_features';
	
	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\ProductFeatureLocalization', 'feature_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_products_features_localization', 'as_products_features.id', '=', 'as_products_features_localization.feature_id')
                     ->where('as_products_features_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_products_features_localization.name');
    }

	// Values ​​of the parameters
	public function parameters()
	{
		return $this->hasMany('App\Models\ProductOption' , 'feature_id');
	}
}
