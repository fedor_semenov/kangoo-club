<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamLocalization extends Model
{
    protected $table = 'as_team_localization';

	protected $guarded = [];
}
