<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainSlider extends Model
{
    protected $table = 'as_main_slider';
	
	protected $guarded = [];

	// Upload images slider
    public static function upload_images( $images )
    {
    	$errors  = '';
    	$counter = 1;

		foreach( $images as $image )
		{
			$validator = \Validator::make( ['images' => $image], ['images' => 'required|image']);
			
			if( $validator->passes() )
			{
				$image_name = time() . '_' . \Slug::make(basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension())) . '.' . $image->getClientOriginalExtension();

				$image_gd = \Image::make( $image );

				if( $image_gd->width() > 960 )
					$image_gd->widen( 960 )->save( 'uploads/main_slider/' . $image_name );
				else
					$image->move( public_path('uploads/main_slider/'), $image_name );

				self::create(['image' => $image_name]);
			}
			else
				$errors = $validator->messages();

			$counter++;
		}
		
		return $errors;
	}
}
