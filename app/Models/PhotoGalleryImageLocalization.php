<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoGalleryImageLocalization extends Model
{
    protected $table = 'as_photo_gallery_images_localization';

	protected $guarded = [];
}
