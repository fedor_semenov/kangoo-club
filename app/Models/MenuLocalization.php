<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuLocalization extends Model
{
    protected $table = 'as_menu_localization';
	
	protected $guarded = [];
}
