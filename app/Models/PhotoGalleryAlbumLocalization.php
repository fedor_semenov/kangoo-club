<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoGalleryAlbumLocalization extends Model
{
    protected $table = 'as_photo_gallery_albums_localization';
	
	protected $guarded = [];
}
