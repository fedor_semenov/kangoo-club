<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageLocalization extends Model
{
    protected $table = 'as_pages_localization';

	protected $guarded = [];
}
