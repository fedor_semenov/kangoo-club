<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Language;
use App\Models\PartnerLocalization;

class Partner extends Model
{
    protected $table = 'as_partners';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\PartnerLocalization', 'partner_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_partners_localization', 'as_partners.id', '=', 'as_partners_localization.partner_id')
                     ->where('as_partners_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_partners_localization.name');
    }

    // Upload images
    public static function upload_images( $images )
    {
		$errors  = '';

		$languages = Language::get();

		foreach( $images as $image )
		{
			$validator = \Validator::make( ['images' => $image], ['images' => 'required|image']);
			
			if( $validator->passes() )
			{
				$image_name = time() . '_' . \Slug::make(basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension())) . '.' . $image->getClientOriginalExtension();

				$image_gd = \Image::make( $image );

				$image_gd->heighten( 100 )->save( 'uploads/partners/' . $image_name );

				$image_current = self::create(['image' => $image_name]);

				foreach( $languages as $language )
		        {
		            PartnerLocalization::create(['partner_id' => $image_current->id, 'lang' => $language->code]);
		        }
			}
			else
				$errors = $validator->messages();
		}
		
		return $errors;
    }
}
