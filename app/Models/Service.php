<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'as_services';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\ServiceLocalization', 'service_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_services_localization', 'as_services.id', '=', 'as_services_localization.service_id')
                     ->where('as_services_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_services_localization.name');
    }

	public function scopeLocalAll( $query, $lang )
	{
		return $query->local($lang)
    				 ->addSelect(
    				 				'as_services_localization.name',
                                    'as_services_localization.annotation',
    				 				'as_services_localization.body',
                                    'as_services_localization.meta_title',
                                    'as_services_localization.meta_keywords',
                                    'as_services_localization.meta_description'
    				 			);  
    }

    // Upload image
    public static function imageUpload( $type, $file, $id )
    {
        $post = self::find( $id );

        if( $post->{$type} )
            \File::delete( public_path('uploads/services/' . $type . '/' . $post->{$type}) );
        
        $new_image = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();

        $file->move( public_path('uploads/services/' . $type . '/'), $new_image );
        
        $post->update([$type => $new_image]);
    }
}
