<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class News extends Model
{
    protected $table = 'as_news';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\NewsLocalization', 'post_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_news_localization', 'as_news.id', '=', 'as_news_localization.post_id')
                     ->where('as_news_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_news_localization.name');
    }

	public function scopeLocalAll( $query, $lang )
	{
		return $query->local($lang)
    				 ->addSelect(
    				 				'as_news_localization.name',
    				 				'as_news_localization.h1',
                                    'as_news_localization.annotation',
    				 				'as_news_localization.body',
    				 				'as_news_localization.meta_title',
    				 				'as_news_localization.meta_keywords',
    				 				'as_news_localization.meta_description'
    				 			);  
    }

    // Upload image
    public static function imageUpload( $file, $id )
    {
        $post = self::find( $id );

        if( $post->image )
            \File::delete( public_path('uploads/news/' . $post->image) );
        
        $new_image = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();

        $image_gd = \Image::make( $file );

        if( $image_gd->width() > 255 )
            $image_gd->widen( 255 )->save( 'uploads/news/' . $new_image );
        else
            $file->move( public_path('uploads/news/'), $new_image );
        
        $post->update(['image' => $new_image]);
    }

    // Get correct date
    public static function getDate( $datetime )
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $datetime)->format('d.m.y');
    }
}
