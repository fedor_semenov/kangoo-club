<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'as_settings';

	protected $guarded = [];

	// Return settings object
	public static function getSettings()
	{
		$all_settings = self::all();
        $settings = [];

        if( count($all_settings) )
        {
        	foreach( $all_settings as $item ) 
	        {
	            $settings[$item->setting_name] = $item->setting_value;
	        }
        }

        return (object)$settings;
	}

	// Update settings
	public static function updateSettings( $settings ) 
	{
        foreach( $settings as $s_key => $s_value ) 
        {
        	self::updateOrCreate(['setting_name' => $s_key], ['setting_value' => (!empty($s_value) ? $s_value : '')]);
        }
	}

	// Google map url
	public static function googleMapUrl( $string )
	{
		$string = preg_replace("'<span[^>]*?>.*?</span>'si", '', $string);
		$string = trim($string);

		return 'https://www.google.com/maps?q=' . urlencode($string);
	} 
}
