<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Language;
use App\Models\PhotoGalleryImageLocalization;

class PhotoGalleryImage extends Model
{
    protected $table = 'as_photo_gallery_images';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\PhotoGalleryImageLocalization', 'image_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_photo_gallery_images_localization', 'as_photo_gallery_images.id', '=', 'as_photo_gallery_images_localization.image_id')
                     ->where('as_photo_gallery_images_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_photo_gallery_images_localization.name');
    }

    // Upload images
    public static function upload_images( $images, $album_id )
    {
		$errors  = '';

		$languages = Language::get();

		foreach( $images as $image )
		{
			$validator = \Validator::make( ['images' => $image], ['images' => 'required|image']);
			
			if( $validator->passes() )
			{
				$image_name = time() . '_' . \Slug::make(basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension())) . '.' . $image->getClientOriginalExtension();

				$image_gd = \Image::make( $image );

				$image_gd->widen( 750 )->save( 'uploads/photo-gallery/images/' . $image_name );
				$image_gd->widen( 360 )->save( 'uploads/photo-gallery/images/middle/' . $image_name );

				$image_current = self::create(['album_id' => $album_id, 'image' => $image_name]);

				foreach( $languages as $language )
		        {
		            PhotoGalleryImageLocalization::create(['image_id' => $image_current->id, 'lang' => $language->code]);
		        }
			}
			else
				$errors = $validator->messages();
		}
		
		return $errors;
    }
}
