<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOptionLocalization extends Model
{
    protected $table = 'as_products_options_localization';
	
	protected $guarded = [];
}
