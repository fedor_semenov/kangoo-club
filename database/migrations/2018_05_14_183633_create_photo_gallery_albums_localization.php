<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoGalleryAlbumsLocalization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('as_photo_gallery_albums_localization', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('album_id')->unsigned();
            $table->foreign('album_id')->references('id')->on('as_photo_gallery_albums')->onDelete('cascade')->onUpdate('cascade');
            $table->string('lang', 2);
            $table->foreign('lang')->references('code')->on('as_languages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->string('h1');
            $table->text('body');
            $table->string('meta_title');
            $table->string('meta_keywords');
            $table->string('meta_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('as_photo_gallery_albums_localization');
    }
}
