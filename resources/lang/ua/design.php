<?php

return [

	// Global
	'home'						=> 'Головна',
	'callback'					=> 'Замовити зворотній дзвінок',
	'callback_title'			=> 'Замовлення зворотного дзвінка',
	'order_product'				=> 'Замовлення товару',
	'sitemap'					=> 'Карта сайту',
	'all_rights_reserved'		=> 'всі права захищені',
	'site_creation'				=> 'створення сайту',
	'subscribe_title'			=> 'Підпишіться на нашу розсилку',
	'subscribe_description'		=> 'і ми щотижня зможемо радувати вас новинами',

	// Main
	'more_information'			=> 'Більше інформації',
	'more'						=> 'Детальніше',
	'shoe'						=> 'Черевики',
	'price'						=> 'Ціна',
	'currency'					=> '€',
	'section_kj_title'			=> 'Що являють собою черевики KJ?',
	'section_kj_description'	=> 'Наведіть на область, яка цікавить Вас, щоб отримати інформацію',
	'section_cb_title'			=> 'Цікавлять якісь питання?',
	'section_cb_description'	=> 'Просто напишіть свої дані, ми Вам зателефонуємо і відповімо на всі питання',
	'section_cb_notice'			=> '* наші консультанти подзвонять Вам протягом декількох хвилин',
	'callback_text'				=> 'Якщо у Вас є питання, Ви можете замовити зворотний дзвінок і ми Вам на них відповімо :)',
	'section_gallery_title'		=> 'Фото наших залів',
	'map_title'					=> 'Де ми знаходимося?',
	'contacts'					=> 'Контакти',
	'services'					=> 'Послуги',
	'section_video_title'		=> 'Відео презентація Kangoo Jump',
	'section_video_btn_text'	=> 'Дивитися відео',

	// About
	'about_title_1'				=> 'Наші переваги',
	'about_title_2'				=> 'Ще трохи про нас',
	'about_cb_title'			=> 'Запишіться на тренування',
	'about_cb_description'		=> 'І отримаєте море емоцій і вражень',
	'about_cb_before_phone'		=> 'Ви можете записатися по телефону:',
	'about_cb_after_phone'		=> 'Або замовте зворотний дзвінок і ми передзвонимо Вам',
	'about_information'			=> 'Інформація',

	// Price
	'price_before'				=> 'Тут Ви можете ознайомитися з нашим прайсом',
	'price_after'				=> '* всі ціни є актуальними',
	'price_cb_text'				=> 'Тепер, коли Ви ознайомилися з цінами,<br/> Ви можете замовити безкоштовну консультацію по телефону і ми відповімо Вам на всі питання',

	// 404
	'error_404_title'			=> 'Вибачте, сторінка упригала',
	'error_404_description'		=> 'Ми теж її чекаємо, а поки Ви можете досліджувати інші розділи нашого сайту',

	// Gallery
	'gallery_album'					=> 'Альбом',
	'gallery_album_select'			=> 'Перейти до вибору альбомів',
	'gallery_album_select_photo'	=> 'Виберіть альбом для перегляду фотографій',
	'gallery_watch_video'			=> 'Також Ви можете подивитися нашу відеогалерею',
	'gallery_album_select_video'	=> 'Виберіть альбом для перегляду відео',
	'gallery_watch_photo'			=> 'Також Ви можете подивитися нашу фотогалерею',

	// Reviews
	'reviews_description'		=> 'Враження наших задоволених клієнтів',
	'reviews_form_title'		=> 'Залишити відгук',

	// Partners
	'partners_description'		=> 'Організації, які з нами співпрацюють',

	// Search
	'search_title'				=> 'Результати пошуку за запитом',

	// Form
	'input_search'				=> 'Пошук по сайту',
	'input_email'				=> 'Ваш e-mail',
	'input_name'				=> 'Ваше ім&#8217;я',
	'input_first_name'			=> 'Ім&#8217;я',
	'input_last_name'			=> 'Фамілія',
	'input_phone'				=> 'Номер телефону',
	'textarea_comment'			=> 'Напишіть текст коментаря',
	'btn_consultation'			=> 'Отримати консультацію',
	'btn_order_consultation'	=> 'Замовити консультацію',
	'btn_services_all'			=> 'Дізнатися більше про послуги',
	'btn_order'					=> 'Замовити',
	'btn_products_all'			=> 'Переглянути весь асортимент',
	'btn_gallery_all'			=> 'Перейти у фотогалерею',
	'btn_subscribe'				=> 'Підписатися',
	'btn_callback'				=> 'Передзвоніть мені',
	'btn_show_information'		=> 'Розкрити інформацію',
	'btn_hide_information'		=> 'Сховати інформацію',
	'btn_roll_up'				=> 'Сховати',
	'btn_read_more'				=> 'Читати повністю',
	'btn_back'					=> 'Повернутися на головну',
	'btn_show_more'				=> 'Показати ще',
	'btn_show_comment'			=> 'Розкрити коментар',
	'btn_hide_comment'			=> 'Приховати коментар',
	'btn_add_comment'			=> 'Залишити коментар',
	'btn_more'					=> 'Дізнатися більше',
	'btn_to_record'				=> 'До запису',

	// Messages
	'message_feedback'			=> 'Ваше повідомлення відправлено. Чекайте, з Вами зв&#8217;яжуться',
	'message_order'				=> 'Ваше замовлення прийнято. Чекайте, з Вами зв&#8217;яжуться',
	'message_review'			=> 'Дякуємо за Ваш відгук, він буде опублікований відразу ж після перевірки модератором',
	'subscribe_success'			=> 'Ви підписалися на нас'
];