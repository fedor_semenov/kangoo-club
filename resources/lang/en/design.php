<?php

return [

	// Global
	'home'						=> 'Home',
	'callback'					=> 'Request callback',
	'callback_title'			=> 'Call back order',
	'order_product'				=> 'Order product',
	'sitemap'					=> 'Sitemap',
	'all_rights_reserved'		=> 'all rights reserved',
	'site_creation'				=> 'site creation',
	'subscribe_title'			=> 'Subscribe to our newsletter',
	'subscribe_description'		=> 'and every week we will be able to please you with news',

	// Main
	'more_information'			=> 'More information',
	'more'						=> 'More',
	'shoe'						=> 'Shoe',
	'price'						=> 'Price',
	'currency'					=> '€',
	'section_kj_title'			=> 'What are the shoes KJ?',
	'section_kj_description'	=> 'Hover over the area of ​​interest to get information',
	'section_cb_title'			=> 'Interested in any questions?',
	'section_cb_description'	=> 'Just write your data, we will call you back and answer all your questions',
	'section_cb_notice'			=> '* our consultants will call you within a few minutes',
	'callback_text'				=> 'If you have any questions, you can order a callback and we will answer them to you :)',
	'section_gallery_title'		=> 'Photos of our halls',
	'map_title'					=> 'Where are we?',
	'contacts'					=> 'Contacts',
	'services'					=> 'Services',
	'section_video_title'		=> 'Video presentation Kangoo Jump',
	'section_video_btn_text'	=> 'Watch video',

	// About
	'about_title_1'				=> 'Our advantages',
	'about_title_2'				=> 'A little more about us',
	'about_cb_title'			=> 'Sign up for a workout',
	'about_cb_description'		=> 'And get a sea of emotions and impressions',
	'about_cb_before_phone'		=> 'You can register by phone:',
	'about_cb_after_phone'		=> 'Or order a return call and we will call you back',
	'about_information'			=> 'Information',

	// Price
	'price_before'				=> 'Here you can see our price list',
	'price_after'				=> '* all prices are up-to-date',
	'price_cb_text'				=> 'Now that you have read the prices,<br/> you can order a free consultation by phone and we will answer all your questions',

	// 404
	'error_404_title'			=> 'Sorry, page jumped',
	'error_404_description'		=> 'We are also waiting for it, but for now you can explore other sections of our site',

	// Gallery
	'gallery_album'					=> 'Album',
	'gallery_album_select'			=> 'Go to album selection',
	'gallery_album_select_photo'	=> 'Select an album to view photos',
	'gallery_watch_video'			=> 'Also you can see our video gallery',
	'gallery_album_select_video'	=> 'Select an album to watch the video',
	'gallery_watch_photo'			=> 'Also you can see our photo gallery',

	// Reviews
	'reviews_description'		=> 'Impressions of our satisfied customers',
	'reviews_form_title'		=> 'Give feedback',

	// Partners
	'partners_description'		=> 'Organizations that cooperate with us',

	// Search
	'search_title'				=> 'Search results for',

	// Form
	'input_search'				=> 'Site search',
	'input_email'				=> 'Your e-mail',
	'input_name'				=> 'Your name',
	'input_first_name'			=> 'First name',
	'input_last_name'			=> 'Last name',
	'input_phone'				=> 'Phone number',
	'textarea_comment'			=> 'Write the comment text',
	'btn_consultation'			=> 'Get consultation',
	'btn_order_consultation'	=> 'Order consultation',
	'btn_services_all'			=> 'Learn more about each service',
	'btn_order'					=> 'Order',
	'btn_products_all'			=> 'See the whole range',
	'btn_gallery_all'			=> 'Go to photo gallery',
	'btn_subscribe'				=> 'Subscribe',
	'btn_callback'				=> 'Call me back',
	'btn_show_information'		=> 'Show information',
	'btn_hide_information'		=> 'Hide information',
	'btn_roll_up'				=> 'Roll up',
	'btn_read_more'				=> 'Read more',
	'btn_back'					=> 'Go back to the home',
	'btn_show_more'				=> 'Show more',
	'btn_show_comment'			=> 'Show comment',
	'btn_hide_comment'			=> 'Hide comment',
	'btn_add_comment'			=> 'Leave a comment',
	'btn_more'					=> 'Learn more',
	'btn_to_record'				=> 'To record',

	// Messages
	'message_feedback'			=> 'Your message has been sent. Wait, you will be contacted',
	'message_order'				=> 'Your order is accepted. Wait, you will be contacted',
	'message_review'			=> 'Thanks for your feedback, it will be published immediately after the moderator checks it',
	'subscribe_success'			=> 'You subscribed to us'
];