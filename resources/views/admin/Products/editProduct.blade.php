@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/products-menu') }}">Магазин</a></li>
				<li><a href="{{ asset('master/products') }}">Товары</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>

	@if( Request::segment(3) != 'create' )
		<style>
			.btn-group.special {
				display: flex;
			}

			.special .btn {
				flex: 1
			}
		</style>
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="btn-group special" role="group">
							<a href="{{ asset('master/products-images/' . (int)Request::segment(3)) }}" class="btn btn-lg btn-info"><i class="fa fa-picture-o"></i> Изображения товара</a>
							<a href="{{ asset('master/products-features-options/' . (int)Request::segment(3)) }}" class="btn btn-lg btn-primary"><i class="fa fa-cogs"></i> Характеристики товара</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif

	@if(Session::has('success'))
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			</div>
		</div>
	@endif
	
	@if( $errors->any() )
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif

	<div class="row">
		<div class="col-md-12">
		
			{!! Form::open(['url' => $rest_api['url'], 'method' => $rest_api['method'], 'class' => 'form-horizontal', 'role' => 'form']) !!}
				<div class="form-group">
					{!! Form::label('visible', 'Отображать товар', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::select('visible', [0 => 'Нет', 1 => 'Да'], $post->visible, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('active', 'Показывать на главной', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::select('active', [0 => 'Нет', 1 => 'Да'], $post->active, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('price', 'Цена', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::text('price',  $post->price, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
										<a href="#{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
									</li>
								@endif
							@endforeach
						</ul>
					</div>
				</div>
				<!-- Tab panes -->
				<div class="tab-content">
					@foreach( $languages as $k_lang => $v_lang )
						@if( $v_lang->visible )
							<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="{{ $v_lang->code }}">
								<div class="form-group">
									{!! Form::label($v_lang->code . '[name]', 'Название', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-8">
										{!! Form::text($v_lang->code . '[name]', $data[$v_lang->code]->name, ['class' => 'form-control']) !!}
									</div>
								</div>
							</div>
						@endif
					@endforeach
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>

@stop