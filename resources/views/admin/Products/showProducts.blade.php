@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/products-menu') }}">Магазин</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="{{ asset('master/products/create') }}" class="thumbnail text-center">
				<i class="fa fa-plus-circle fa-5x"></i>
				<div class="title">Добавить товар</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($products) > 0 )
				<div class="title-module">
					<div class="pull-left">
						<strong>Название</strong>
					</div>
					<div class="pull-right">
						<strong class="text-control">Управление</strong>
					</div>
					<div class="clear"></div>
				</div>
				
				{!! Form::open(['url' => asset('master/products/destroy'), 'method' => 'DELETE', 'class' => 'products-list']) !!}
					<div class="list-group">
						@foreach($products as $product)
							<div class="list-group-item" id="{{ $product->id }}">
								<div class="pull-left">
									<span class="item-element-menu menu-sortable" title="Сортировка товаров">
										<i class="fa fa-bars"></i>
									</span>
									<span class="item-element-menu">
										<input type="checkbox" name="check[]" value="{{ $product->id }}" />
									</span>
									<span class="item-element-menu">
										{{ $product->name }}
									</span>
								</div>
								<div class="pull-right">
									<div class="btn-group" role="group">
										<button type="button" class="btn {{ !empty($product->visible) ? 'btn-success' : 'btn-default' }} visible" title="Показать/скрыть товар" data-id="{{ $product->id }}">
											<i class="fa {{ !empty($product->visible) ? 'fa-star' : 'fa-star-o' }}"></i>
										</button>
										<button type="button" class="btn {{ !empty($product->active) ? 'btn-danger' : 'btn-default' }} active" title="Показывать товар на главной странице" data-id="{{ $product->id }}">
											<i class="fa fa-check"></i>
										</button>
										<a href="{{ asset('master/products-images/' . $product->id) }}" class="btn btn-info" title="Изображения товара"><i class="fa fa-picture-o"></i></a>
										<a href="{{ asset('master/products-features-options/' . $product->id) }}" class="btn btn-primary" title="Характеристики товара"><i class="fa fa-cogs"></i></a>
										<a href="{{ asset('master/products/' . $product->id) }}" class="btn btn-warning" title="Редактировать товар"><i class="fa fa-pencil"></i></a>
										<button type="button" class="btn btn-danger delete" title="Удалить товар" data-id="{{ $product->id }}"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						@endforeach
					</div>
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>	
				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Товары еще не добавлены</div>
			@endif

		</div>
	</div>
@stop

@section('scripts')
    @parent
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script>
		$(function(){
			// Удаление товара
			$('.delete').click( function() {
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('.list-group-item').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').attr('selected', true);
				$(this).closest('form').submit();
			});

			// Удаление товаров
			$('form.products-list').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});

			// Показать/скрыть товар
			$('.visible').on('click', function(event){
				event.preventDefault();
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default').find('i').removeClass('fa-star').addClass('fa-star-o');
				else
					$(this).removeClass('btn-default').addClass('btn-success').find('i').removeClass('fa-star-o').addClass('fa-star');
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), action: 'visible' };
				$.post('{{ asset("master/products/toggle") }}', data, function(data){ console.log(data) }, 'JSON');
			});

			// Показать услугу на главной странице
			$('.active').on('click', function(event){
				event.preventDefault();
				if( $(this).hasClass('btn-danger') )
					$(this).removeClass('btn-danger').addClass('btn-default');
				else
					$(this).removeClass('btn-default').addClass('btn-danger');
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), action: 'active' };
				$.post('{{ asset("master/products/toggle") }}', data, function(data){ console.log(data) }, 'JSON');
			});
			
			// Позиционирование товаров
			$('.list-group').sortable({
				handle: '.menu-sortable',
				opacity: 0.7,
				stop: function(){
					var data = { _token: '{{ csrf_token() }}', position: $(this).sortable('toArray') };
					$.post('{{ asset("master/products/sortable") }}', data, function(data){ console.log(data) }, 'JSON');	
				}
			});
		});
	</script>
@endsection