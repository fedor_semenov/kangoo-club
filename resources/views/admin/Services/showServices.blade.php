@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="{{ asset('master/services/create') }}" class="thumbnail text-center">
				<i class="fa fa-plus-circle fa-5x"></i>
				<div class="title">Добавить услугу</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($services) )
				<div class="title-module">
					<div class="pull-left">
						<strong>Название</strong>
					</div>
					<div class="pull-right">
						<strong class="text-control">Управление</strong>
					</div>
					<div class="clear"></div>
				</div>
				
				{!! Form::open(['method' => 'DELETE', 'url' => asset('master/services/destroy'), 'class' => 'services-list']) !!}
					<div class="list-group">
						
						@foreach( $services as $item )
							<div class="list-group-item" id="{{ $item->id }}">
								<div class="pull-left">
									<span class="item-element-menu menu-sortable" title="Сортировка услуг">
										<i class="fa fa-bars"></i>
									</span>
									<span class="item-element-menu">
										<input type="checkbox" name="check[]" value="{{ $item->id }}" />
									</span>
									<span class="item-element-menu">
										{{ $item->name }}
									</span>
								</div>
								<div class="pull-right">
									<div class="btn-group" role="group">
										<button type="button" class="btn {{ !empty($item->visible) ? 'btn-success' : 'btn-default' }} visible" title="Показать/скрыть услугу" data-id="{{ $item->id }}">
											<i class="fa {{ !empty($item->visible) ? 'fa-star' : 'fa-star-o' }}"></i>
										</button>
										<button type="button" class="btn {{ !empty($item->active) ? 'btn-danger' : 'btn-default' }} active" title="Показывать услугу на главной странице" data-id="{{ $item->id }}">
											<i class="fa fa-check"></i>
										</button>
										<a href="{{ asset('services/' . $item->slug) }}" class="btn btn-primary" title="Открыть в новом окне" target="_blank"><span class="glyphicon glyphicon-share-alt"></span></a>
										<a href="{{ asset('master/services/' . $item->id) }}" class="btn btn-warning" title="Редактировать услугу"><i class="fa fa-pencil"></i></a>
										<button type="button" class="btn btn-danger delete" title="Удалить услугу" data-id="{{ $item->id }}"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						@endforeach
						
					</div>
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>	
				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Услуги еще не добавлены</div>
			@endif
		</div>
	</div>
@stop

@section('styles')
	@parent
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('scripts')
    @parent
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script>
		$(function(){
			
			// Удаление услуги
			$('.delete').click( function() {
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('.list-group-item').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').attr('selected', true);
				$(this).closest('form').submit();
			});

			// Удаление услуг
			$('form.services-list').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Показать/скрыть услугу
			$('.visible').on('click', function(event){
				
				event.preventDefault();
				
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default').find('i').removeClass('fa-star').addClass('fa-star-o');
				else
					$(this).removeClass('btn-default').addClass('btn-success').find('i').removeClass('fa-star-o').addClass('fa-star');
				
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), action: 'visible' };
				$.post('{{ asset("master/services/toggle") }}', data, function(data){ console.log(data) }, 'JSON');
			});

			// Показать услугу на главной странице
			$('.active').on('click', function(event){
				
				event.preventDefault();
				
				if( $(this).hasClass('btn-danger') )
					$(this).removeClass('btn-danger').addClass('btn-default');
				else
					$(this).removeClass('btn-default').addClass('btn-danger');
				
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), action: 'active' };
				$.post('{{ asset("master/services/toggle") }}', data, function(data){ console.log(data) }, 'JSON');
			});
			
			// Позиционирование услуг
			$('.list-group').sortable({
				handle: '.menu-sortable',
				opacity: 0.7,
				stop: function(){
					var data = { _token: '{{ csrf_token() }}', position: $(this).sortable('toArray') };
					$.post('{{ URL::to("master/services/sortable") }}', data, function(data){ console.log(data) }, 'JSON');	
				}
			});
		});
	</script>
@endsection