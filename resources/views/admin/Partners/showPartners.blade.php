@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">

			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
				
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			
		</div>
	</div>
	
	{!! Form::open(['files' => true, 'class' => 'images-upload']) !!}
		<div class="row">
			<div class="col-sm-10">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-btn">
							<span class="btn btn-primary btn-file">
								<i class="fa fa-picture-o"></i> <input type="file" name="images[]" accept="image/gif, image/jpeg, image/png" multiple />
							</span>
						</span>
						<input type="text" class="form-control" placeholder="Выберите изображения..." readonly>
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<button type="submit" class="btn btn-success btn-block">Загрузить</button>
			</div>
		</div>
	{!! Form::close() !!}
	
	{!! Form::open(['method' => 'DELETE', 'url' => asset('master/partners/destroy'), 'class' => 'images-list']) !!}
		@if( count($partners) > 0 )
			<div class="row">
				<div class="images">
					@foreach( $partners as $partner )
						<div class="col-sm-3" id="{{ $partner->id }}">
							<div class="panel panel-default">
								<div class="panel-body">
									<img src="{{ asset('uploads/partners/' . $partner->image) }}" alt="" class="item-sortable" />
								</div>
								<div class="panel-footer text-center">
									<input type="checkbox" name="check[]" value="{{ $partner->id }}" />
									<div class="btn-group" role="group">
										<button type="button" class="btn {{ !empty($partner->visible) ? 'btn-success' : 'btn-default' }} visible-partner" title="Показать/скрыть партнёра" data-id="{{ $partner->id }}">
											<i class="fa {{ !empty($partner->visible) ? 'fa-star' : 'fa-star-o' }}"></i>
										</button>
										<a href="{{ asset('master/partners/' . $partner->id) }}" class="btn btn-warning" title="Редактировать данные о патрнёре"><i class="fa fa-pencil"></i></a>
										<button type="button" class="btn btn-danger delete" title="Удалить партнёра" data-id="{{ $partner->id }}">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>
				</div>
			</div>
		@else
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-warning">Партнёры еще не добавлены</div>
				</div>
			</div>
		@endif
	{!! Form::close() !!}
@stop

@section('scripts')
    @parent
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="{{ asset('admin/js/matchHeight.min.js') }}"></script>
	<script>
		$(function(){
			
			// Удаление партнёра
			$('.delete').click(function(){
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('.panel').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').prop('selected', true);
				$(this).closest('form').submit();
			})

			// Удаление патрнёров
			$('form.images-list').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});

			// Показать/скрыть партнёра
			$('.visible-partner').on('click', function(event){
				event.preventDefault();
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id') };
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default').find('i').removeClass('fa-star').addClass('fa-star-o');
				else
					$(this).removeClass('btn-default').addClass('btn-success').find('i').removeClass('fa-star-o').addClass('fa-star');
				$.post('{{ asset("master/partners/visible") }}', data, function(data){ console.log(data) }, 'JSON');
			});

			// Позиционирование изображений
			$('.images').sortable({
				handle: '.item-sortable',
				opacity: 0.7,
				stop: function(){
					var data = { _token: '{{ csrf_token() }}', position: $(this).sortable('toArray') };
					$.post('{{ asset("master/partners/sortable") }}', data, function(data){ console.log(data) }, 'JSON');

					// HeightEqual image
					$('.panel .panel-body').matchHeight();
				}
			});

			// File input
			$(document).on('change', '.btn-file :file', function(){
				var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				input.trigger('fileselect', [numFiles, label]);
			});
			$(document).ready(function(){
				$('.btn-file :file').on('fileselect', function(event, numFiles, label){
					var input = $(this).parents('.input-group').find(':text'),
						log = numFiles > 1 ? numFiles + ' файла(ов) выбрано' : label;
					
					if( input.length )
						input.val(log);
					else
						if( log ) 
							alert(log);
				});
			});

			// HeightEqual image
			$('.panel .panel-body').matchHeight();
		});
	</script>
@endsection