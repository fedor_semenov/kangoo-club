<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Панель администратора - "{{ $title }}"</title>
    <link rel="icon" type="image/svg+xml" href="{{ asset('favicon.svg') }}">
    
	<!-- Styles -->
	@section('styles')
		<!-- Bootstrap Core CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- Custom Fonts -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Custom CSS -->
		<link href="{{ asset('admin/css/dashboard.css') }}" rel="stylesheet">
	@show
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ asset('master/settings') }}" data-toggle="tooltip" data-placement="right" title="Настройки"><i class="fa fa-cogs"></i></a>
			</div>
			<ul class="nav navbar-right top-nav">
				<li>
					<a href="{{ asset('/') }}" target="_blank" class="link-site" data-toggle="tooltip" data-placement="left" title="Сайт"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
				</li>
				<li>
					<a href="{{ route('logout') }}" class="logout"><i class="fa fa-sign-out"></i>&ensp;Выход</a>
				</li>
			</ul>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav dash-menu">
					<li class="logo">
						<a href="{{ asset('master') }}">
							<img src="{{ asset('admin/img/logo.svg') }}" alt="logo" />
						</a>
					</li>
					
					@include('admin._menu')
					
				</ul>
			</div>
		</nav>
        <div id="page-wrapper">
            <div class="container-fluid">
				
				@yield('main')
				
            </div>
        </div>
    </div>
	
	<!-- Scripts -->
	@section('scripts')
		<!-- jQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script>
			// Tooltip
			$('[data-toggle="tooltip"]').tooltip();
			
			// Alerts
			if( $('.alert').length )
			{
				setTimeout(function(){
					$('.alert-success, .alert-danger').fadeOut('fast')
				}, 3000);
			}
		</script>
	@show
</body>
</html>