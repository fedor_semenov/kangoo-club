@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/reviews') }}">Отзывы</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
		
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			
			{!! Form::open(['method' => 'PUT', 'class' => 'form-horizontal', 'role' => 'form']) !!}
				<div class="form-group">
					{!! Form::label('visible', 'Одобрить отзыв', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::select('visible', [0 => 'Нет', 1 => 'Да'], $post->visible, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('first_name', 'Имя', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('first_name',  $post->first_name, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('last_name', 'Фамилия', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('last_name',  $post->last_name, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('comment', 'Отзыв', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::textarea('comment',  $post->comment, ['class' => 'form-control', 'rows' => 5]) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>

@stop