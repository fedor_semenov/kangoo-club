<li @if( Request::segment(2) == 'menu' ) class="active" @endif>
	<a href="{{ asset('master/menu') }}"><i class="fa fa-bars"></i> Меню</a>
</li>
<li @if( Request::segment(2) == 'pages' ) class="active" @endif>
	<a href="{{ asset('master/pages') }}"><i class="fa fa-file"></i> Страницы</a>
</li>
<li @if( Request::segment(2) == 'services' ) class="active" @endif>
	<a href="{{ asset('master/services') }}"><i class="fa fa-wrench"></i> Услуги</a>
</li>
<li @if( Request::segment(2) == 'news' ) class="active" @endif>
	<a href="{{ asset('master/news') }}"><i class="fa fa-sticky-note"></i> Новости</a>
</li>
<li @if( Request::segment(2) == 'articles' ) class="active" @endif>
	<a href="{{ asset('master/articles') }}"><i class="fa fa-sticky-note"></i> Статьи</a>
</li>
<li @if( Request::segment(2) == 'team' ) class="active" @endif>
	<a href="{{ asset('master/team') }}"><i class="fa fa-user"></i> Наша команда</a>
</li>
<li @if( Request::segment(2) == 'stock' ) class="active" @endif>
	<a href="{{ asset('master/stock') }}"><i class="fa fa-percent"></i> Акции</a>
</li>
<li @if( substr(Request::segment(2), 0, 13) == 'photo-gallery' ) class="active" @endif>
	<a href="{{ asset('master/photo-gallery-albums') }}"><i class="fa fa-picture-o"></i> Фотогалерея</a>
</li>
<li @if( substr(Request::segment(2), 0, 13) == 'video-gallery' ) class="active" @endif>
	<a href="{{ asset('master/video-gallery-albums') }}"><i class="fa fa-video-camera"></i> Видеогалерея</a>
</li>
<li @if( Request::segment(2) == 'partners' ) class="active" @endif>
	<a href="{{ asset('master/partners') }}"><i class="fa fa-trademark"></i> Партнёры</a>
</li>
<li @if( Request::segment(2) == 'certificates' ) class="active" @endif>
	<a href="{{ asset('master/certificates') }}"><i class="fa fa-certificate"></i> Сертификаты</a>
</li>
<li @if( substr(Request::segment(2), 0, 8) == 'products' ) class="active" @endif>
	<a href="{{ asset('master/products-menu') }}"><i class="fa fa-shopping-cart"></i> Магазин</a>
</li>
<li @if( Request::segment(2) == 'subscribers' ) class="active" @endif>
	<a href="{{ asset('master/subscribers') }}"><i class="fa fa-paper-plane"></i> Рассылка писем</a>
</li>
<li @if( Request::segment(2) == 'reviews' ) class="active" @endif>
	<a href="{{ asset('master/reviews') }}"><i class="fa fa-comments"></i> Отзывы @if( $reviews_count ) <span class="label label-success">{{ $reviews_count }}</span> @endif</a>
</li>
<li @if( Request::segment(2) == 'feedbacks' ) class="active" @endif>
	<a href="{{ asset('master/feedbacks') }}" id="count-messages"><i class="fa fa-envelope"></i> Обратная связь @if( $feedbacks_count ) <span class="label label-danger">{{ $feedbacks_count }}</span> @endif</a>
</li>
<li @if( Request::segment(2) == 'users' ) class="active" @endif>
	<a href="{{ asset('master/users') }}"><i class="fa fa-users"></i> Пользователи</a>
</li>