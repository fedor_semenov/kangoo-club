@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>
	</div>

	<!-- Schedule -->
	<iframe id="schedule" src="https://kangoclub.otmechalka.com/foreign/schedule"></iframe>
	
	@if( $page->body )
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection

@section('styles')
	@parent
	<style>
		#schedule {
			width: 100%;
			outline: none;
			border: 1px solid #999;
		}
	</style>
@endsection

@section('scripts')
	@parent
	<script src="http://kangoclub.otmechalka.com/scripts/iframeResizer.min.js"></script>
	<script>
		iFrameResize({ checkOrigin: false }, '#schedule');
	</script>
@endsection