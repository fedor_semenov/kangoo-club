@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title not-margin">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			    <div class="page-description">{{ trans('design.gallery_album_select_photo') }}</div>
			    
			    @if( count($albums) )
					<ul class="gallery-albums">
						@include('frontend.components._gallery_albums', ['type' => 'photo'])
					</ul>
				@endif

			</div>
		</div>
	</div>
	@if( count($albums) > 8 )
		<a href="#" class="btn-albums-more" data-type="photo" data-offset="1">
			<span>{{ trans('design.btn_show_more') }}</span>
		</a>
	@endif
	
	@if( $page->body )
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection