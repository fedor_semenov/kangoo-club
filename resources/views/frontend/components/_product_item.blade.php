<div class="product-item">
    <div class="p-middle-images">
        @forelse( $p_value->product_images as $pi_key => $pi_value )
            <div>
                <a href="{{ asset('uploads/products/' . $pi_value->image) }}">
                    <img src="{{ asset('uploads/products/middle/' . $pi_value->image) }}" alt="{{{ $p_value->name }}} - {{ $pi_key + 1 }}" />
                </a>
            </div>
        @empty
            <div>
                <a href="https://via.placeholder.com/290x290">
                    <img src="https://via.placeholder.com/290x290" alt="{{{ $p_value->name }}}" />
                </a>
            </div>
        @endforelse
    </div>
    <div class="p-thumbs-images">
        @forelse( $p_value->product_images as $pi_key => $pi_value )
            <div>
                 <img src="{{ asset('uploads/products/thumbs/' . $pi_value->image) }}" alt="{{{ $p_value->name }}} - {{ $pi_key + 1 }}" />
            </div>
        @empty
            <div>
                 <img src="https://via.placeholder.com/50x55" alt="{{{ $p_value->name }}}" />
            </div>
        @endforelse
    </div>
    <canvas id="p-line-{{ $p_value->id }}" width="300" height="2"></canvas>
    <div class="p-title">{{ $p_value->name }}</div>
    @if( count($p_value->product_features) )
        <ul class="p-features">
            {!! App\Models\Product::renderProductFeatures( $p_value->product_features ) !!}
        </ul>
    @endif
    <div class="p-price" data-product-price="{{ (int)$p_value->price }}">
        <div class="f-left">
            <div class="p-name">{{ trans('design.price') }}</div>
            <div class="p-number">
                {{ number_format((int)$p_value->price, 0, ' ', ' ') }} <span>{{ trans('design.currency') }}</span>
            </div>
        </div>
        <div class="f-right">
            <a href="#order" class="btn-order">{{ trans('design.btn_order') }}</a>
        </div>
        <div class="clear-fix"></div>
    </div>
</div>