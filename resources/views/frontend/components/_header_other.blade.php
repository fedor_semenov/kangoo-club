<div class="container">
    <div class="row">
        <div class="col-lg-2 col-xl-2">
            <!-- Logo -->
            @if( $front_page )
                <div class="logo">
            @else
                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}" class="logo">
            @endif
                @include('frontend.components._logo')
            @if( $front_page )
                </div>
            @else
                </a>
            @endif
        </div>
        <div class="col-lg-9 col-xl-9">
            @if( count($menu) )
                <!-- Menu -->
                <nav class="menu">
                    <ul>
                        @foreach( $menu as $item )
                            @if( $item->slug )
                                @if( isset($item->children) && count($item->children) > 0 )
                                    <li class="sub-menu">
                                        {!! App\Models\Menu::itemMenu( $item->name, $item->slug ) !!}
                                        <ul>
                                            @foreach( $item->children as $child )
                                                <li>{!! App\Models\Menu::itemMenu( $child->name, $child->slug ) !!}</li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li>{!! App\Models\Menu::itemMenu( $item->name, $item->slug ) !!}</li>
                                @endif
                            @endif
                        @endforeach
                    </ul>
                    <button class="menu-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </nav>
            @endif
        </div>
        <div class="col-lg-1 col-xl-1">
            @if( count($languages) > 1 )
                <!-- Languages -->
                <ul class="languages f-right">
                    @foreach( $languages as $language )
                        @if( app()->getLocale() != $language->code )
                            <li><a href="{{ LaravelLocalization::getLocalizedURL($language->code) }}" title="{{ $language->name }}">{{ $language->alias }}</a></li>
                        @else
                            <li><span title="{{ $language->name }}">{{ $language->alias }}</span></li>
                        @endif
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-3 col-xl-3">
            <!-- Phone box -->
            <div class="phone-box">
                @if( $settings->phone )
                    <a href="tel:{{ str_replace(' ', '', $settings->phone) }}" class="phone">{{ $settings->phone }}</a>
                @endif
                <a href="#callback" class="callback">{{ trans('design.callback') }}</a>
            </div>
        </div>
        <div class="col-md-8 col-lg-4 col-xl-4">
            <!-- Search -->
            {!! Form::open(['url' => LaravelLocalization::getLocalizedURL(app()->getLocale(), 'search'), 'method' => 'GET', 'class' => 'search']) !!}
                <input type="text" name="s" placeholder="{{ trans('design.input_search') }}" @if( $search_string = Request::get('s') )value="{{ $search_string }}"@endif required />
                <button type="submit"></button>
            {!! Form::close() !!}
        </div>
        <div class="col-md-9 col-lg-5 col-xl-5">
            <!-- Address -->
            @if( $settings->{'address_1_' . app()->getLocale()} || $settings->{'address_2_' . app()->getLocale()} )
                <ul class="address f-right">
                    <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_1_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_1_' . app()->getLocale()} !!}</a></li>
                    <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_2_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_2_' . app()->getLocale()} !!}</a></li>
                </ul>
            @endif
        </div>
    </div>
</div>