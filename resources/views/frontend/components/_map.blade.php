<div class="contacts-map">
    <div class="cm-title">{{ trans('design.map_title') }}</div>
    @if( $settings->{'address_1_' . app()->getLocale()} || $settings->{'address_2_' . app()->getLocale()} )
        <ul class="cm-addresses">
            <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_1_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_1_' . app()->getLocale()} !!}</a></li>
            <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_2_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_2_' . app()->getLocale()} !!}</a></li>
        </ul>
    @endif
    <div id="map"></div>
    <script>
        var map, marker, locations;
        
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 47.86034895, lng: 35.11066321},
                zoom: 14,
                disableDefaultUI: true
            });

            locations = [{
                position: {lat: 47.8483034, lng: 35.1357273},
                icon: {
                    url: "{{ asset('frontend/img/map_dot_icon_1.png') }}"
                }
            },
            {
                position: {lat: 47.860782, lng: 35.106582},
                icon: {
                    url: "{{ asset('frontend/img/map_dot_icon_2.png') }}"
                }
            }];

            locations.forEach( function( item ) {
                marker = new google.maps.Marker({
                    position: item.position,
                    map: map,
                    icon: item.icon
                });
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq9gnL2dPu6GZofx-xqZytishiFSgqwbU&callback=initMap" async defer></script>
</div>