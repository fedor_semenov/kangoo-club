@extends('frontend.layout')

@section('main')
	
	<div class="reviews">
		<div class="container">
	    	<div class="row">
				<div class="col-12">
					
					<!-- Breadcrumbs -->
					@include('frontend.components._breadcrumbs')
					
					<h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
				    <div class="page-description">{{ trans('design.reviews_description') }}</div>
				</div>
			</div>
			@if( count($reviews) )
				@foreach( $reviews as $r_key => $r_value )
					@if( (($r_key + 1) % 3) == 1 )
						<div class="row">
					@endif
					<div class="col-lg-4 col-xl-4">
						<div class="review-item-container">
							<div class="review-item">
								<div class="ri-name">{{ $r_value->first_name . ' ' . $r_value->last_name }}</div>
								<div class="ri-date">{{ date('d.m.y', strtotime($r_value->created_at)) }}</div>
								<div class="ri-text">
									{!! $r_value->comment !!}
								</div>
								<a href="#" class="ri-more" data-text-show="{{ trans('design.btn_show_comment') }}" data-text-hidden="{{ trans('design.btn_hide_comment') }}">{{ trans('design.btn_show_comment') }}</a>
							</div>
						</div>
					</div>
					@if( (($r_key + 1) % 3) == 0 || ($r_key + 1) == $reviews->count() )
						</div>
					@endif
				@endforeach
				<div class="row">
					<div class="col-12">
						{!! $reviews->render() !!}
					</div>
				</div>
			@endif
		</div>
		{!! Form::open() !!}
			<input type="hidden" name="locale" value="{{ app()->getLocale() }}">
			<div class="container">
	    		<div class="row">
					<div class="col-12">
						<div class="page-title">{{ trans('design.reviews_form_title') }}</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 offset-lg-2 col-xl-3 offset-xl-3">
						<input type="text" name="first_name" placeholder="{{ trans('design.input_first_name') }}" required />
					</div>
					<div class="col-lg-4 col-xl-3">
						<input type="text" name="last_name" placeholder="{{ trans('design.input_last_name') }}" required />
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
						<textarea name="comment" placeholder="{{ trans('design.textarea_comment') }}" required></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<button type="submit">{{ trans('design.btn_add_comment') }}</button>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>

	@if( $page->body )
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection