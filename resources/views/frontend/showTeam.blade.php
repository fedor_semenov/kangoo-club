@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>
		
		@if( count($team) )
			<div class="team">
				@foreach( $team as $t_key => $t_value )
					@if( (($t_key + 1) % 3) == 1 )
						<div class="row">
					@endif
					<div class="col-lg-4 col-xl-4">
						<div class="t-item" @if( $t_value->image )style="background-image: url({{ asset('uploads/team/' . $t_value->image) }});"@endif>
							<div class="ti-label">{{ $t_value->role }}</div>
							<div class="ti-name">
								<div class="tin-first">{{ $t_value->first_name }}</div>
								<div class="tin-last">{{ $t_value->last_name }}</div>
							</div>
						</div>
					</div>
					@if( (($t_key + 1) % 3) == 0 || ($t_key + 1) == $team->count() )
						</div>
					@endif
				@endforeach
			</div>
		@endif
		
		@if( $page->body )
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		@endif
	</div>

@endsection