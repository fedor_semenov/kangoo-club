@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>
	</div>

	@if( count($products) )
		<div class="products">
			<div class="container">
				@foreach( $products as $p_key => $p_value )
					@if( (($p_key + 1) % 3) == 1 )
						<div class="row">
					@endif
						<div class="col-lg-4 col-xl-4">
							@include('frontend.components._product_item')
						</div>
					@if( (($p_key + 1) % 3) == 0 || ($p_key + 1) == $products->count() )
						</div>
					@endif
				@endforeach
				<div class="row">
					<div class="col-12">
						{!! $products->render() !!}
					</div>
				</div>
			</div>
		</div>
	@endif

	@if( $page->body )
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection