<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $page->meta_title }}</title>
    @if( $page->meta_keywords )
        <meta name="keywords" content="{{ $page->meta_keywords }}">
    @endif
    @if( $page->meta_description )
        <meta name="description" content="{{ $page->meta_description }}">
    @endif
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <link rel="icon" type="image/svg+xml" href="{{ asset('favicon.svg') }}">
    <!-- Styles -->
    @section('styles')
        <link href="{{ asset('frontend/css/all.css') }}" rel="stylesheet">
    @show
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Pushy Menu -->
    <nav class="pushy pushy-left">
        <div class="pushy-content"></div>
    </nav>
    <!-- Site Overlay -->
    <div class="site-overlay"></div>
    <div class="wrapper" id="container">
        <header class="header {{ ($front_page ? 'main' : 'other') }}">
            
            @if( $front_page )
                @include('frontend.components._header_main')
            @else
                @include('frontend.components._header_other')
            @endif

        </header><!-- .header-->
        <main class="content">
    
             @yield('main')

        </main><!-- .main-->
        <footer class="footer">
            
            @if( $g_map_init )
                @include('frontend.components._map')
            @endif

            <div class="section-bottom">
                <div class="sb-top">
                    <div class="container">
                        <div class="row">
                            <div class="d-none d-lg-block d-xl-block col-lg-2 col-xl-2">
                                <!-- Logo -->
                                @if( $front_page )
                                    <div class="logo">
                                @else
                                    <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}" class="logo">
                                @endif
                                    @include('frontend.components._logo')
                                @if( $front_page )
                                    </div>
                                @else
                                    </a>
                                @endif
                            </div>
                            <div class="d-none d-lg-block d-xl-block col-lg-2 col-xl-2">
                                @if( count($menu) )
                                    <!-- Menu -->
                                    <ul class="menu">
                                        @foreach( $menu as $item )
                                            @if( is_null($item->parent_id) )
                                                <li>{!! App\Models\Menu::itemMenu( $item->name, $item->slug ) !!}</li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <div class="d-none d-lg-block d-xl-block col-lg-2 col-xl-2">
                                @if( count($menu) )
                                    <!-- Menu -->
                                    <ul class="menu">
                                        @foreach( $menu as $item )
                                            @if( isset($item->children) && count($item->children) > 0 )
                                                @foreach( $item->children as $child )
                                                    <li>{!! App\Models\Menu::itemMenu( $child->name, $child->slug ) !!}</li>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <div class="col-md-8 col-lg-3 col-xl-4">
                                @if( $settings->phone || $settings->email || $settings->{'address_1_' . app()->getLocale()} || $settings->{'address_2_' . app()->getLocale()} )
                                    <!-- Contacts -->
                                    <div class="contacts">
                                        <div class="c-title">{{ trans('design.contacts') }}</div>
                                        @if( $settings->phone )
                                            <a href="tel:{{ str_replace(' ', '', $settings->phone) }}" class="c-phone">{{ $settings->phone }}</a>
                                        @endif
                                        @if( $settings->{'address_1_' . app()->getLocale()} || $settings->{'address_2_' . app()->getLocale()} )
                                            <ul class="address">
                                                <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_1_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_1_' . app()->getLocale()} !!}</a></li>
                                                <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_2_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_2_' . app()->getLocale()} !!}</a></li>
                                            </ul>
                                        @endif
                                        @if( $settings->email )
                                            <a href="mailto:{{ $settings->email }}" target="_blank" class="c-mail">{{ $settings->email }}</a>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4 col-lg-3 col-xl-2">
                                <!-- Callback -->
                                <div class="callback-box">
                                    <div class="cb-title">
                                        {{ trans('design.callback_text') }}
                                    </div>
                                    <a href="#callback" class="cb-btn-callback">{{ trans('design.btn_order') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sb-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-lg-6 col-xl-6">
                                @if( $settings->viber || $settings->telegram || $settings->facebook || $settings->instagram )
                                    <ul class="social">
                                        @if( $settings->viber )
                                            <li class="viber">
                                                <a href="viber://chat?number={{ str_replace(' ', '', $settings->viber) }}" target="_blank" title="Viber"></a>
                                            </li>
                                        @endif
                                        @if( $settings->telegram )
                                            <li class="telegram">
                                                <a href="tg://resolve?domain={!! $settings->telegram !!}" title="Telegram"></a>
                                            </li>
                                        @endif
                                        @if( $settings->instagram )
                                            <li class="instagram">
                                                <a href="{!! $settings->instagram !!}" target="_blank" title="Instagram"></a>
                                            </li>
                                        @endif
                                        @if( $settings->facebook )
                                            <li class="facebook">
                                                <a href="{!! $settings->facebook !!}" target="_blank" title="Facebook"></a>
                                            </li>
                                        @endif
                                    </ul>
                                @endif
                                <!-- Sitemap -->
                                <a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'sitemap') }}" class="sitemap">{{ trans('design.sitemap') }}</a>
                            </div>
                            <div class="col-md-7 col-lg-6 col-xl-6">
                                <!-- Subscribe -->
                                {!! Form::open(['url' => asset('subscribe'), 'class' => 'subscribe']) !!}
                                    <input type="hidden" name="locale" value="{{ app()->getLocale() }}" />
                                    <div class="s-title">{{ trans('design.subscribe_title') }}</div>
                                    <div class="s-description">{{ trans('design.subscribe_description') }}</div>
                                    <div class="row">
                                        <div class="col-lg-7 col-xl-7">
                                            <input type="email" name="email" class="input" placeholder="{{ trans('design.input_email') }}" required />
                                        </div>
                                        <div class="col-lg-5 col-xl-5">
                                            <button type="submit">{{ trans('design.btn_subscribe') }}</button>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!-- Copyright -->
                            <div class="copyright f-left">© {{ request()->getHttpHost() }} {{ date('Y') }}, {{ trans('design.all_rights_reserved') }}</div>
                            <!-- Atelier-Soft -->
                            <a href="https://atelier-soft.com" class="atelier-soft f-right" target="_blank">{{ trans('design.site_creation') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer><!-- .footer-->
    </div>
    
    <!-- Callback form -->
    {!! Form::open(['url' => asset('feedback'), 'id' => 'callback', 'class' => 'mfp-hide white-popup-block']) !!}
        <input type="hidden" name="locale" value="{{ app()->getLocale() }}" />
        <div class="title">{{ trans('design.callback_title') }}</div>
        <input type="text" name="name" placeholder="{{ trans('design.input_name') }}" required />
        <input type="tel" name="phone" placeholder="{{ trans('design.input_phone') }}" required />
        <button type="submit">{{ trans('design.btn_callback') }}</button>
    {!! Form::close() !!}
    
    <!-- Order form -->
    {!! Form::open(['url' => asset('products'), 'id' => 'order', 'class' => 'mfp-hide white-popup-block']) !!}
        <div class="title">{{ trans('design.order_product') }} <span></span></div>
        <input type="hidden" name="locale" value="{{ app()->getLocale() }}" />
        <input type="hidden" name="product_name" value="" />
        <input type="hidden" name="product_price" value="" />
        <input type="text" name="user_name" placeholder="{{ trans('design.input_name') }}" required />
        <input type="tel" name="user_phone" placeholder="{{ trans('design.input_phone') }}" required />
        <button type="submit">{{ trans('design.btn_order') }}</button>
    {!! Form::close() !!}
    
    <!-- Scripts -->
    @section('scripts')
        <script src="{{ asset('frontend/js/all.js') }}"></script>
    @show
</body>
</html>
