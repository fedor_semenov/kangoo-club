@extends('frontend.layout')

@section('main')
	
	<div class="error-404">
		<div class="e-content">
			<img src="{{ asset('frontend/img/error_404.png') }}" alt="" class="e-img" />
			<div class="e-title">{{ trans('design.error_404_title') }}</div>
			<div class="e-description">{{ trans('design.error_404_description') }}</div>
			<a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}" class="e-back">{{ trans('design.btn_back') }}</a>
		</div>
	</div>

@endsection