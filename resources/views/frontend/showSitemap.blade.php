@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			    
			    @if( count($pages) )
				    <ul class="sitemap">
				    	@foreach( $pages as $p_key => $p_value )
					    	@if( $p_value->slug == 'services' )
						    	<li @if( count($services) )class="childs"@endif>
						    		<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug) }}">{{ $p_value->name }}</a>
						    		@if( count($services) )
							    		<ul>
							    			@foreach( $services as $service )
							    				<li><a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug . '/' . $service->slug) }}">{{ $service->name }}</a></li>
							    			@endforeach
							    		</ul>
							    	@endif
						    	</li>
						    @elseif( $p_value->slug == 'news' )
						    	<li @if( count($news) )class="childs"@endif>
						    		<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug) }}">{{ $p_value->name }}</a>
						    		@if( count($news) )
							    		<ul>
							    			@foreach( $news as $post )
							    				<li><a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug . '/' . $post->slug) }}">{{ $post->name }}</a></li>
							    			@endforeach
							    		</ul>
							    	@endif
						    	</li>
						   	@elseif( $p_value->slug == 'articles' )
						    	<li @if( count($articles) )class="childs"@endif>
						    		<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug) }}">{{ $p_value->name }}</a>
						    		@if( count($articles) )
							    		<ul>
							    			@foreach( $articles as $article )
							    				<li><a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug . '/' . $article->slug) }}">{{ $article->name }}</a></li>
							    			@endforeach
							    		</ul>
							    	@endif
						    	</li>
						    @elseif( $p_value->slug == 'stock' )
						    	<li @if( count($stock) )class="childs"@endif>
						    		<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug) }}">{{ $p_value->name }}</a>
						    		@if( count($stock) )
							    		<ul>
							    			@foreach( $stock as $item )
							    				<li><a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug . '/' . $item->slug) }}">{{ $item->name }}</a></li>
							    			@endforeach
							    		</ul>
							    	@endif
						    	</li>
						    @elseif( $p_value->slug == 'photo-gallery' )
						    	<li @if( count($photo_gallery) )class="childs"@endif>
						    		<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug) }}">{{ $p_value->name }}</a>
						    		@if( count($photo_gallery) )
							    		<ul>
							    			@foreach( $photo_gallery as $pg )
							    				<li><a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug . '/' . $pg->slug) }}">{{ $pg->name }}</a></li>
							    			@endforeach
							    		</ul>
							    	@endif
						    	</li>
						    @elseif( $p_value->slug == 'video-gallery' )
						    	<li @if( count($video_gallery) )class="childs"@endif>
						    		<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug) }}">{{ $p_value->name }}</a>
						    		@if( count($video_gallery) )
							    		<ul>
							    			@foreach( $video_gallery as $vg )
							    				<li><a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $p_value->slug . '/' . $vg->slug) }}">{{ $vg->name }}</a></li>
							    			@endforeach
							    		</ul>
							    	@endif
						    	</li>
						   	@else
						   		<li><a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), (empty($p_value->slug) ? '/' : $p_value->slug)) }}">{{ $p_value->name }}</a></li>
						    @endif
				    	@endforeach
				    </ul>
				@endif
			</div>
		</div>
	</div>

@endsection