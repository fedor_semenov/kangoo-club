<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
	<p>Имя: <strong>{{ $name }}</strong></p>
	<p>Номер телефона: <strong><a href="tel:{{ str_replace(['(', ')', ' ', '-'], '', $phone) }}">{{ $phone }}</a></strong></p>
</body>
</html>