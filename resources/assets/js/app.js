// iOS detected
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

// AJAX send form
function ajaxSend( form, modalClose = false ){
	
	var currentLanguage = $('html').attr('lang'),
		textSuccess = (currentLanguage == 'ua') ? 'Дякуємо' : (currentLanguage == 'en') ? 'Thank' : 'Спасибо',
		textError = (currentLanguage == 'ua') ? 'Помилка' : (currentLanguage == 'en') ? 'Error' : 'Ошибка';

	$( form ).submit(function(event){
		event.preventDefault();
		
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			dataType: 'JSON',
			data: $(this).serialize(),
			success: function(data){
				
				if( data.success )
				{
					$(form)[0].reset();

					if( modalClose )
						$.magnificPopup.close();

					swal(textSuccess, data.success, 'success');
				}
			},
			error: function(data){
					
				var errors = data.responseJSON.errors,
					errorsHTML = '';
				
				$.each(errors, function(index, value) {
					errorsHTML += value + "<br/>";
				});
				
				swal(textError, errorsHTML, 'error');
			}
		});
	});
}

// DOM
$(function(){

	// Phones mask
	$('input[type="tel"]').mask('+38 (999) 999-99-99');

	// More info scroll
	$('.btn-more-info').click(function(event){
		event.preventDefault();
		$.scrollTo('#section-what', 800);
	});

	// Header slider
	$('.header .slider').dddSlider({ autoplay: false });

	// Mobile menu
	if( $('.header .menu > ul').length )
	{
		var mobileMenu = '<ul>';

		$('.header .menu > ul > li').each(function(){
			
			if( $(this).find('ul').length )
			{
				mobileMenu += '<li class="pushy-submenu pushy-submenu-closed">';
					
					if( $(this).find('> a').length )
						mobileMenu += '<a href="' + $(this).find('> a').attr('href') + '">' + $(this).find('> a').text() + '</a>';
					else
						mobileMenu += '<span>' + $(this).find('> span').text() + '</span>';

					mobileMenu += '<ul>';

						$(this).find('ul li').each(function(){
							mobileMenu += '<li class="pushy-link">';
								
								if( $(this).find('a').length )
									mobileMenu += '<a href="' + $(this).find('a').attr('href') + '">' + $(this).find('a').text() + '</a>';
								else
									mobileMenu += '<span>' + $(this).find('span').text() + '</span>';
							
							mobileMenu += '</li>';
						});

					mobileMenu += '</ul>';
				mobileMenu += '</li>';
			}
			else
			{
				mobileMenu += '<li class="pushy-link">';
					
					if( $(this).find('> a').length )
						mobileMenu += '<a href="' + $(this).find('> a').attr('href') + '">' + $(this).find('> a').text() + '</a>';
					else
						mobileMenu += '<span>' + $(this).find('> span').text() + '</span>';

				mobileMenu += '</li>';
			}
		});

		mobileMenu += '</ul>';

		$('.pushy-content').append( mobileMenu );

		// Toggle pushy submenu
		if( $('.pushy-content .pushy-submenu').length )
		{
			$('.pushy-submenu').on('click', function(event){

		        var selected = $(this);

				if( selected.hasClass('pushy-submenu-closed') ) 
				{
					event.preventDefault();
					
					// Hide opened submenus
					$('.pushy-submenu').addClass('pushy-submenu-closed').removeClass('pushy-submenu-open');
		            
					// Show submenu
					selected.removeClass('pushy-submenu-closed').addClass('pushy-submenu-open');
		        }
		        else
		        {
		            // Hide submenu
					selected.addClass('pushy-submenu-closed').removeClass('pushy-submenu-open');
		        }
		    });
		}
	}

	// Shoe canvas lines
	if( $('.section-shoe').length )
	{
		var coordinates = {
							first:  [
										{ moveTo: [0, 0], lineTo: [110, 0] }, 
										{ moveTo: [120, 0], lineTo: [350, 0] }
									],
							
							second: [
										{ moveTo: [0, 0], lineTo: [40, 0] }, 
										{ moveTo: [50, 0], lineTo: [130, 0] }
									],
							
							third:  [
										{ moveTo: [0, 0], lineTo: [40, 0] }, 
										{ moveTo: [50, 0], lineTo: [130, 0] }
									],
							
							fourth: [
										{ moveTo: [0, 0], lineTo: [95, 0] }, 
										{ moveTo: [105, 0], lineTo: [150, 0] }
									],
							
							sixth:  [
										{ moveTo: [0, 0], lineTo: [90, 0] }, 
										{ moveTo: [100, 0], lineTo: [205, 0] }
									],

							seventh:[
										{ moveTo: [0, 0], lineTo: [4, 0] }, 
										{ moveTo: [10, 0], lineTo: [145, 0] }, 
										{ moveTo: [155, 0], lineTo: [235, 0] }
									]
						  };

		$.each(coordinates, function(key, value) {
		
			var canvas = document.getElementById(key + '-box-line'),
				context = '';
				
			if( canvas )
			{
				context = canvas.getContext('2d');

				$.each(value, function(i_key, i_val) {
				
					context.beginPath();                        

					$.each(i_val, function(j_key, j_val) {

						if( j_key == 'moveTo' )
							context.moveTo(j_val[0], j_val[1]);                         
						
						if( j_key == 'lineTo' )
							context.lineTo(j_val[0], j_val[1]);
					});

					context.lineWidth = 4;      
					context.strokeStyle = '#fff';    
					context.stroke();
				});
			}
		});
	}

	// Main video
	$('.section-video .svp-btn').on('click touchstart', function(){
		$('.section-video .sv-play').remove();
		$('.section-video').removeClass('no-active');
		
		if( !iOS )
			$('.section-video').find('iframe')[0].src += '&autoplay=1';
	});

	// Products
	var productsMainSlider = $('.products-main').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: false,
			infinite: false,
			variableWidth: true,
			responsive: [
				{
					breakpoint: 1199,
					settings: {
						slidesToShow: 2,
						variableWidth: false
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						variableWidth: false
					}
				},
			]
		});

	$('.pm-controls .slide-prev').click(function(){
		productsMainSlider.slick('slickPrev');
	});

	$('.pm-controls .slide-next').click(function(){
		productsMainSlider.slick('slickNext');
	});

	if( $('.product-item').length )
	{
		
		$('.product-item').each(function(){

			// Images
			$(this).find('.p-middle-images').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				swipe: false,
				asNavFor: $(this).find('.p-thumbs-images')
			});
			$(this).find('.p-thumbs-images').slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: false,
				asNavFor: $(this).find('.p-middle-images'),
				focusOnSelect: true,
				variableWidth: true
			});
			$(this).find('.p-middle-images').magnificPopup({
				delegate: 'a',
				type: 'image',
				tLoading: 'Loading image #%curr%...',
				mainClass: 'mfp-img-mobile',
				closeOnContentClick: true,
            	closeBtnInside: false,
            	fixedContentPos: false,
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0, 1], // Will preload 0 - before current, and 1 after the current image
					tCounter: ''
				},
				image: {
					tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
				}
	        });

			// Canvas lines
			var canvas = document.getElementById( $(this).find('canvas').attr('id') ),
				context = canvas.getContext('2d');

			context.beginPath();
			context.moveTo(0, 0); 
			context.lineTo(33, 0);
			context.lineWidth = 4;      
			context.strokeStyle = '#fdd147';  
			context.stroke();

			context.beginPath();
			context.moveTo(45, 0);
			context.lineTo(220, 0);
			context.lineWidth = 4;      
			context.strokeStyle = '#fdd147';  
			context.stroke();

			context.beginPath();
			context.moveTo(236, 0);
			context.lineTo(300, 0);
			context.lineWidth = 4;      
			context.strokeStyle = '#fdd147';  
			context.stroke();
		});
	}

	// Main photo gallery
	if( $('.section-photo-gallery').length )
	{
		var carousel = $('#carousel').waterwheelCarousel({
			separation: 275,
			sizeMultiplier: 0.85,
			flankingItems: 2,
			opacityMultiplier: 1,
			imageNav: false
		});

		$('.carousel-container .slide-prev').click(function(){
			carousel.prev();
		});

		$('.carousel-container .slide-next').click(function(){
			carousel.next();
		});
	}

	// Callback
	$('.callback, .btn-consultation, .cb-btn-callback, .cb-callback').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: 'input[name="name"]',
		fixedContentPos: false
	});
	$(document).on('click', '.mfp-btn-form', function(event) {
		event.preventDefault();
		$.magnificPopup.open({
			items: {
				src: $('#callback')
			},
			type: 'inline',
			preloader: false,
			focus: 'input[name="name"]',
			fixedContentPos: false
		});
	});

	// Product order
	$('.btn-order').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: 'input[name="user_name"]',
		fixedContentPos: false,
		callbacks: {
			beforeOpen: function() {

				var product_name = this.st.el.closest('.product-item').find('.p-title').text(),
					product_price = parseInt(this.st.el.closest('.product-item').find('.p-price').data('product-price'));

				$('#order .title span').text('"' + product_name + '"');
				$('#order input[name="product_name"]').val(product_name);
				$('#order input[name="product_price"]').val(product_price);
			}
		}
	});

	// Gallery photo
	$('.gallery-items .photo').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		fixedContentPos: false,
		image: {
			verticalFit: true
		},
		callbacks: {
			open: function() {
				$('.mfp-content').append('<a href="#" class="mfp-btn-form">Получить консультацию</a>');
			}
		}
	});

	// Gallery video
	$('.gallery-items .video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});

	// Show/hidden review
	$('.review-item-container .ri-more').click(function(event){
		event.preventDefault();

		var el = $(this);

		$('.review-item').each(function(){
			$(this).not(el.parent()).removeClass('show');
			$(this).find('.ri-more').text( $(this).find('.ri-more').data('text-show') );
		});
		
		el.parent().toggleClass('show');

		if( el.parent().hasClass('show') ) 
			el.text( el.data('text-hidden') );
		else 
			el.text( el.data('text-show') );
	});

	// Certificates
	$('.certificates .c-item').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		fixedContentPos: false,
		image: {
			verticalFit: true
		}
	});

	// ----------------------------------------------- AJAX -----------------------------------------------
	// Forms callback
	ajaxSend('#callback', true);
	ajaxSend('.a-callback');
	ajaxSend('.price form');
	ajaxSend('.section-callback');

	// Form order
	ajaxSend('#order', true);

	// Form review
	ajaxSend('.reviews form');

	// Subscribe
	ajaxSend('.subscribe');

	// Gallery albums more
	$('.btn-albums-more').click(function(event){
		event.preventDefault();

        $.ajax({
			url: '/' + $(this).data('type') + '-gallery',
			type: 'POST',
			dataType: 'JSON',
			data: { 'offset': parseInt($(this).data('offset')), 'locale': $('html').attr('lang'), '_token': $('meta[name="csrf-token"]').attr('content') },
			success: function(response){
            	if( response.albums.length )
            	{
					$('.gallery-albums').append(response.albums);
					$('.btn-albums-more').data('offset', response.offset);

					if( $('.gallery-albums li').length == response.albums_count )
						$('.btn-albums-more').remove();
            	}
			}
		});
	});
	// ----------------------------------------------------------------------------------------------------
});